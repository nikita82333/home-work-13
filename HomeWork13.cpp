﻿
#include <iostream>
#include "Helpers.h"

int main()
{
    double a = 3, b = 4;
    double c = squareOfSum(a, b);

    std::cout << "(" << a << " + " << b << ")^2 = " << c << std::endl;

    return 0;
}

